//or if i can use timeout function

var startTimeout = 1;
var endTimeout = 100;

console.log('start:' + new Date());
function defineTimeoutLoop(interval) {
    if (interval <= endTimeout)
        window.setTimeout(function () {
            console.log('interval:' + new Date());
            console.log(interval);
            defineTimeoutLoop(++interval);
        }, interval * 1000);
}
defineTimeoutLoop(startTimeout);

//or without
var start = 1;
var end = 100;

var beginDate = new Date();
console.log('start:' + new Date());
var interval = start;
while (interval <= end) {
    var seconds = (new Date().getTime() - beginDate.getTime()) / 1000;
    if (seconds == interval) {
        console.log('interval:' + new Date());
        console.log(interval);
        beginDate = new Date();
        interval++;
    }


console.log('end:' + new Date());


