// I think that the first closure is not so needed so we can directly write like this
function closureFunc(val) {
    return function () { return val; };
}
function getArray() {
    const arr = [];
    for (var i = 0; i < 10; i++)
        arr.push(closureFunc(i));
    return arr;
}

console.log('Make sure that closure()[3]() === 3', getArray()[3]() === 3)


//for EcmaScript 6 and higher we can juste change to let
function closure() {
    return (function () {
        const arr = []
        for (let i = 0; i < 10; i++) {
            arr.push(() => i)
        }
        return arr;
    })()
}

console.log('Make sure that closure()[3]() === 3', closure()[3]() === 3)


//for EcmaScript 5 and smaller we can just change the push func
function closure2() {
    return (function () {
        const arr = []
        for (var i = 0; i < 10; i++) {
            arr.push((function outer(i) {
                return function inner() {
                    return i;
                }
            })(i))
        }
        return arr;
    })()
}

console.log('Make sure that closure()[3]() === 3', closure2()[3]() === 3)

